There are many different ways to deploy Concerto, and the method you choose will depend on your technical expertise and project requirements. This quickstart guide provides a simple route to installation but we would not recommend this method for use in any 'production' environment (i.e. testing real people). 

Hosting a web application on the public server in an efficient and secure manner requires specialist knowledge. There are many moving parts involved and considerations to make. We refer administrators and more advanced users to the [Concerto Deployment Guide](https://github.com/campsych/concerto-platform/wiki/deployment-guide) which describes the many different deployment options for real-world scenarios.

Generic installation instructions
---------------------------------
This could be done on your local computer or any networked machine running Linux, Windows or MacOS.

1. Install **[docker](https://docs.docker.com/install/)** (v1.13 or newer). You may need to Sign Up for a DockerID.
2. Install **[docker-compose](https://docs.docker.com/compose/install/#install-compose)** (v1.21 or newer)
3. Download [docker-compose.yml](https://raw.githubusercontent.com/campsych/concerto-platform/master/deploy/docker-compose/docker-compose.yml) file and save to a path of your choice
4. Edit docker-compose to use any Concerto stable release image of your choice (e.g. **5.0**). **master** or branches with ***-dev** suffix are considered unstable.

       concerto:
          image: campsych/concerto-platform:5.0

5. Start it:

       docker-compose up -d

Installation on Amazon EC2
--------------------------
This will create a single instance running Concerto inside a Virtual Private Cloud. As long as the default instance type is used (t2.micro) it should fall within [Amazon Free Tier](https://aws.amazon.com/free/) if you are eligible. Please note you are solely responsible for any charges your usage may incur. 

1. Create an [Amazon AWS account](https://aws.amazon.com/) and sign-in to the Console
2. [Create a Key Pair](https://eu-west-2.console.aws.amazon.com/ec2/v2/home#KeyPairs:sort=keyName)
3. [Launch ec2-basic CloudFormation Stack](https://console.aws.amazon.com/cloudformation/home#/stacks/new?stackName=concerto-ec2-basic&templateURL=https://s3-eu-west-1.amazonaws.com/campsych/concerto/cf/concerto-ec2-basic.yml). In 'Stack details' section you have to set passwords for both Concerto 'admin' account and database. Best to use random ones. Also set Key Pair to the one you've just created. 
4. Once the stack is created you will find the URL to your instance in the Outputs tab.

### With a domain name and TLS certificate

This variant is better if you own a domain name and are familiar with how to set DNS records for it. It will create the same cloud resources as above but it will additionally provision a free certificate from [Let's Encrypt](https://letsencrypt.org). This means that traffic between the clients and the server will be encrypted.

1. Create an [Amazon AWS account](https://aws.amazon.com/) and sign-in to the Console (as above)
2. [Create a Key Pair](https://eu-west-2.console.aws.amazon.com/ec2/v2/home#KeyPairs:sort=keyName) (as above)
3. [Launch ec2-basic-tls CloudFormation Stack](https://console.aws.amazon.com/cloudformation/home#/stacks/new?stackName=concerto-ec2-basic-tls&templateURL=https://s3-eu-west-1.amazonaws.com/campsych/concerto/cf/concerto-ec2-basic-tls.yml) instead
2. Once the stack is created there will be an IP address shown in the Outputs tab. Take note of it.
3. Go to your domain registrar's panel and create or change the `A` DNS record for your domain to point to this IP address. For example, here's how to do it in [Godaddy](https://uk.godaddy.com/help/change-an-a-record-19239) or [Bluehost](https://my.bluehost.com/hosting/help/559)

It can take a while until your domain resolves to Concerto server which would allow to issue a valid certificate. Before that happens you can still access Concerto. Your web browser **will** complain though about the certificate being not issued by trusted Certificate Authority. This is normal and does not interfere with encryption of the traffic itself.

Running Administration Panel
----------------------------

When the installation process has completed, you should be able to access the Concerto Platform administration panel by going to: [http://localhost/admin](http://localhost/admin)

Default login and password is:

Login: **admin**
Password: **admin**

**Please ensure that you immediately change your login and password when logging in for the first time!**

This does not apply if you've used CloudFormation templates on AWS. In that case the password is whatever you've set it up to be during creation of the stack.

Troubleshooting
---------------

### 0.0.0.0:80: bind: address already in use

Port needed for Concerto's web server (80) is already taken by something else that is running on your system. You could change the ports that Concerto uses by editing the `docker-compose.yml` file. Alternatively, find out what is using the port and move it elsewhere.

Example - changing Concerto web server port mapping to port 8080:

    ports:
      - 8080:80

The above command will make Concerto available on [http://localhost:8080/](http://localhost:8080/)

### Does not work on Raspberry PI

ARM architecture is not supported, mostly because Concerto uses a MySQL database. It might work with a different database that works on ARM, such as Postgres. You would need to rebuild the container yourself using our Dockerfile as a starting point.

### Version in `./docker-compose.yml` is unsupported

If you see this message then you are not using the most up-to-date version of Docker in your system. Please follow the link above to the Docker installation guide in order to update it.

### The paths X are not shared from OS X and are not known to Docker. You can configure shared paths from Docker -> Preferences... -> File Sharing

By default, on Mac OS, Docker does not permit any volumes outside of the user's home directory to be mounted. You need to follow the instructions given regarding changing preferences in order to explicitly allow the directory, or consider pulling the Concerto files from somewhere within your user's home directory (e.g. `~/concerto`).

